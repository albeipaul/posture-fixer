import React, { useEffect, useRef, useState } from "react";
import "./App.css";
import ml5 from "ml5";
import GaugeChart from "./GaugeChart";
import useInterval from "./useInterval";

let classifier;

function App() {
  const [showNotification, setShowNotification] = useState(false);
  const videoRef = useRef();
  const [gaugeData, setGaugeData] = useState([0.5, 0.5]);
  const [shouldClassify, setShouldClassify] = useState(false);

  useEffect(() => {
    classifier = ml5.imageClassifier("./my-model/model.json", () => {
      navigator.mediaDevices
        .getUserMedia({ video: true, audio: false })
        .then(stream => {
          videoRef.current.srcObject = stream;
          videoRef.current.play();
        });
    });
  }, []);

  useInterval(() => {
    if (classifier && shouldClassify) {
      classifier.classify(videoRef.current, (error, results) => {
        if (error) {
          console.error(error);
          return;
        }

        results.sort((a, b) => b.label.localeCompare(a.label));
        setGaugeData(results.map(entry => entry.confidence));

        if (results[1].confidence > results[0].confidence || results[0].confidence < 0.7) {
          new Notification("Correct your posture");
        }
      });
    }
  }, 30000);

  function notifyMe() {
    if (!("Notification" in window)) {
      alert("This browser does not support desktop notification");
    }
  
    else if (Notification.permission === "granted") {
      setShowNotification(true);
    }
  
    else if (Notification.permission !== "denied") {
      Notification.requestPermission().then(function (permission) {
        if (permission === "granted") {
          setShowNotification(true);
        }
      });
    }
  }

  return (
    <React.Fragment>
      <h1>
        How good is your posture? <br />
      </h1>
      <GaugeChart data={gaugeData} />
      <button onClick={() => {
        setShouldClassify(!shouldClassify);
        notifyMe();
      }}>
        {shouldClassify ? "Stop" : "Start"}
      </button>
      <video
        ref={videoRef}
        style={{ transform: "scale(-1, 1)" }}
        width="300"
        height="150"
      />
    </React.Fragment>
  );
}

export default App;
